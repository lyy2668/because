var htmlTags = {
    "a": true,
    "body": true,
    "br": true,
    "button": true,
    "canvas": true,
    "center": true,
    "div": true,
    "em": true,
    "font": true,
    "footer": true,
    "form": true,
    "frame": true,
    "h1": true,
    "h2": true,
    "h3": true,
    "h4": true,
    "h5": true,
    "h6": true,
    "head": true,
    "header": true,
    "html": true,
    "i": true,
    "iframe": true,
    "img": true,
    "input": true,
    "label": true,
    "li": true,
    "link": true,
    "meta": true,
    "ol": true,
    "option": true,
    "output": true,
    "p": true,
    "param": true,
    "script": true,
    "section": true,
    "select": true,
    "span": true,
    "strong": true,
    "style": true,
    "table": true,
    "tbody": true,
    "td": true,
    "textarea": true,
    "tfoot": true,
    "th": true,
    "thead": true,
    "time": true, 
    "title": true,
    "tr": true,
    "ul": true
};

var becauseFiles = [];

// Read the because script files url.
var readAllBecauseFiles = function() {
    var becauseScriptFiles = [];
    $("script").each(function() {
        var currentTag = $(this); 
        var fileNameInList = currentTag.attr("src").split(".");
        if(fileNameInList.pop() === "bec") {
            becauseScriptFiles.push(currentTag.attr('src'));
        }
    });
    return becauseScriptFiles;
}; 

// TODO: Remove the uncache!
// Fetch all because script files.
var fetchAllBecauseFiles = function() {
    var becauseScriptFiles = readAllBecauseFiles();
    becauseScriptFiles.forEach(function(file) {
           $.ajax({
            url: file + '?' + (new Date()).toString(),
            type: "get",
            success: parseBecauseFile
        });
    });
    
}

// Collect all because files in mem.
var collectBecauseFile = function() {
    
};

// Begin to parse the because file. Describe the main logic.
var parseBecauseFile = function(becauseFile) {
    var sentences = becauseFile.split(";");
    sentences.forEach(function(sentence) {
        // Parse each sentence.
        sentence = sentence.trim();
        if(sentence !== "") {
            var sentenceParts = sentence.split(":");
            // Check syntax error.
            if(sentenceParts.length != 2) {
                console.error("Wrong syntax! One more ':' detected.");
                return null;
            }
            var reason = sentenceParts[0];
            var consequence = sentenceParts[1];
            var reasonSideEffect = parseReason(reason);
            //var consequenceSideEffect = parseConsequence(consequence, reasonSideEffect);
        }
    });
    
};

// Parse the reason.
var parseReason = function(reason) {
    var reasonParts = reason.split(" ");
    // Check syntax error.
    if(reasonParts[0] !== "because") {
        console.error("Wrong syntax! No keyword 'because' found in statement.");
        return null;        
    }
    
    // Process event.
    var event = reasonParts[1].split(".");
    // Check syntax error.
    if(event.length !== 2) {
        console.error("Wrong syntax! error found when parsing event.");
        return null;        
    }
    var eventTriggerDOM = event[0];
    var triggeredEvent = event[1];
    var triggerDOMType = getDOMType(eventTriggerDOM);
};

// Get the trigger DOM type.
var getDOMType = function(eventTriggerDOM) {
    // If it's a single DOM.
    if(eventTriggerDOM.charAt(0) === "#") {
        return "SINGLEDOM";
    } 
    // If it's a tag name.
    else if(eventTriggerDOM in htmlTags) {
        return "HTMLTAGDOM";
    } 
    // If it's a class of DOMs.
    else if(/^[\w]+$/.test(eventTriggerDOM)) {
        if($("." + eventTriggerDOM)[0]) {
            return "CLASSDOM";    
        }
    }
    // If it's a ids matching regex.
    else if(eventTriggerDOM.charAt(0) === "[" && eventTriggerDOM.charAt(eventTriggerDOM.length - 1) === "]") {
        return "REGEXDOM";
    }    
    else {
        console.log("Wrong syntax! " + eventTriggerDOM + "is a unknown type of event trigger!");
        return undefined;
    }
}

// Parse the consequence.
var parseConsequence = function() {
    
};

fetchAllBecauseFiles();



